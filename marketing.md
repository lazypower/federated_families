# Marketing Family Federation

> How do you sell these services to your family?

We aren't 100% sure but some ideas are below.

# Matrix/Riot.im

* It's cross platform Facetime. You can chat, talk and video chat on all phones, tablets, desktops and laptops
* You can share files, not just pictures
* You can share full resolution pictures
* We can chat no matter where we are across the world/country/etc
* A private chat service that works in real time, but like email. Our converastions on our server are private to us, conversations to other people outside our server are semi private, no ads, tight security features if you want them, and free wifi video/calling.
