# Matrix / riot.im

A self-hosted, federated chat system and so much more.

{:toc}

# Software

- [matrix.org](https://matrix.org/)
- [riot.im](https://about.riot.im/)

# Features

- Text messaging
- Media (images) messaging
- Voice calling
- Video calling
- File transfers
- Communities
- Chat rooms
- riot.im provides cross-platform access to matrix (web, iOS, Android, etc)

# Matrix

- Follow the standard setup guide [here (link)](https://github.com/matrix-org/synapse)
- Be sure to setup Postgres or similar database
- Be sure to setup a turn service for voice/video features
- Consider running your own identity server
    - sydent [(link)](https://github.com/matrix-org/sydent)
    - mxisd [(link)](https://github.com/kamax-io/mxisd)
- Setup as matrix.domain.com as the DNS record (recommended)
- Disable account creation / registration
    - Use ```register_new_matrix_user``` for registering new users

# Riot.im (web access)

- Static html can easily be served with html/apache/nginx
- Consider Matamo [(link)](https://matomo.org/) (formerly piwik) for knowing how many people are using your riot.im deployment
- Setup as riot.domain.com or chat.domain.com as the DNS record(s) (recommended)
- Consider setting up 'The Diner' room for all local users to join for generalized discussion and banter
    - See 'img/matrix_riot/thediner.jpg' for a run room avatar
- Consider settup up 'Support' room for all users to ask questions and help each other
    - See 'img/matrix_riot/help.jpg' for a fun room avatar

### Example riot.im config.domain.com.json
```json
{
    "default_hs_url": "https://matrix.domain.com",
    "default_is_url": "https://matrix.domain.com",
    "disable_custom_urls": true,
    "disable_guests": true,
    "disable_login_language_selector": false,
    "disable_3pid_login": false,
    "brand": "Riot",
    "integrations_ui_url": "",
    "integrations_rest_url": "",
    "bug_report_endpoint_url": "https://riot.im/bugreports/submit",
    "features": {
        "feature_groups": "labs",
        "feature_pinning": "labs"
    },
    "default_federate": true,
    "welcomePageUrl": "home_custom.html",
    "default_theme": "light",
    "roomDirectory": {
        "servers": [
            "matrix.domain.com"
        ]
    },
    "welcomeUserId": "@riot-bot:matrix.org",
    "piwik": {
        "url": "https://piwik.domain.com/",
        "whitelistedHSUrls": ["https://matrix.domain.com"],
        "whitelistedISUrls": ["https://matrix.domain.com"],
        "siteId": 1
    }
}
```

### Example riot.im customized home page
```html
<style type="text/css">

/* we deliberately inline style here to avoid flash-of-CSS problems, and to avoid
 * voodoo where we have to set display: none by default
 */

.mx_HomePage_header h1 {
    margin-left: 0px;
    margin-bottom: 0px;
    margin-top: 20px;
    margin-right: 20px;
    color: #454545;
}

.mx_HomePage_header h2 {
    margin-left: 0px;
    margin-top: 5px;
    margin-bottom: 20px;
    margin-right: 20px;
    color: #454545;
}

.mx_HomePage_header h1 a {
    color: #454545;
}

.mx_HomePage h3 {
    margin-top: 30px;
}

.mx_HomePage_header {
    border: 1px solid #76CFA6;
    background-color: #eaf5f0;
    border-radius: 5px;
    align-items: center;
}

.mx_HomePage_col {
    display: flex;
    flex-direction: row;
}

.mx_HomePage_toprow {
    flex-wrap: wrap;
}

.mx_HomePage_row {
    flex: 1 1 0;
    margin-right: 20px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}

.mx_HomePage_logo {
    margin-top: 20px;
    margin-left: 40px;
    margin-right: 40px;
    margin-bottom: 20px;
    display: inline;
    height: 100px;
}

.mx_HomePage_room {
    cursor: pointer;
    float: left;
    text-decoration: none;
    text-align: center;
    padding-left: 10px;
    padding-right: 10px;
    width: 120px;
}

.mx_HomePage_toprow .mx_HomePage_room {
    width: 64px;
}


.mx_HomePage_room .mx_HomePage_icon {
    border-radius: 50%;
    width: 64px;
    height: 64px;
}

.mx_HomePage_room .mx_HomePage_name {
    display: block;
}

.mx_HomePage_room .mx_HomePage_desc {
    display: block;
    font-size: 12px;
    margin-top: 8px;
}

.mx_HomePage_comment {
    display: flex;
    align-items: center;
    margin-left: 100px;
    min-height: 64px;
}

.mx_HomePage_container h3::after,
.mx_HomePage_container h4::after {
    content: ":";
}

.mx_HomePage_container {
    display: block ! important;
    margin: 20px;
}

.mx_HomePage_container h1,
.mx_HomePage_container h2,
.mx_HomePage_container h3,
.mx_HomePage_container h4 {
    font-weight: 600;
}
</style>

<div class="mx_HomePage_container">
    <div class="mx_HomePage_col mx_HomePage_header">
        <a href="https://riot.domain.com"><img src="home/images/logo.svg" class="mx_HomePage_logo"></a>
        <div>
            <h2>_t("Decentralised, encrypted chat &amp; collaboration powered by [matrix]")</h2>
        </div>
    </div>
    <div class="mx_HomePage_col mx_HomePage_toprow">
        <div class="mx_HomePage_row">
            <div>
                <h3>Find a Community</h3>
                <a class="mx_HomePage_room" href="#/groups">
                    <img class="mx_HomePage_icon" src="img/icons-groups.svg">
                </a>
                <span class="mx_HomePage_comment">
                    Lots of communities already exist. Check them out!
                </span>
            </div>
        </div>
        <div class="mx_HomePage_row">
            <div>
                <h3>Search the room directory</h3>
                <a class="mx_HomePage_room" href="#/directory">
                    <img class="mx_HomePage_icon" src="img/icons-directory.svg">
                </a>
                <span class="mx_HomePage_comment">
                    Lots of rooms already exist. Check out the directory!
                 </span>
            </div>
        </div>
    </div>

    <div class="mx_HomePage_col mx_HomePage_toprow">
        <div class="mx_HomePage_row">
            <div>
                <h3>Get Help</h3>
                <a class="mx_HomePage_room" href="#/room/#support:domain.com">
                    <img class="mx_HomePage_icon" src="img/help.jpg">
                </a>
                <span class="mx_HomePage_comment">
                    Ask a question, get some help.
                </span>
            </div>
        </div>
        <div class="mx_HomePage_row">
            <div>
                <h3>The Diner</h3>
                <a class="mx_HomePage_room" href="#/room/#thediner:domain.com">
                    <img class="mx_HomePage_icon" src="img/thediner.jpg">
                </a>
                <span class="mx_HomePage_comment">
                    Chat with other domain.com users
                 </span>
            </div>
        </div>
    </div>



    <h3>Go Mobile</h3>
        <div class="mx_HomePage_col">
            <div class="mx_HomePage_row">
                <div>
                    <a target="_blank" href="https://itunes.apple.com/us/app/vector.im/id1083446067"><img src="https://about.riot.im/wp-content/themes/riot/img/link2.png" alt=""></a>
               </div>
            </div>
            <div class="mx_HomePage_row">
                <div>
                    <a target="_blank" href="https://play.google.com/store/apps/details?id=im.vector.alpha"><img src="https://about.riot.im/wp-content/themes/riot/img/link3.png" alt=""></a>
                </div>
             </div>
        </div>

    <h3> Desktop App</h3>
        <div class="mx_HomePage_row">
            <a class="mx_HomePage_room" target="_blank" href="https://riot.im/desktop.html"><img src="https://about.riot.im/wp-content/themes/riot/img/distr.png" alt=""></a>
        </div>

    <h3>Full Documentation</h3>
    <a target="_blank" href="https://riot.domain.com/docs">Further documentation and guide</a>

</div>
```