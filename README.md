# Federated Families

A collection of configs, docs and other useful information for bringing federated services to families.

Things like Matrix, Mastodon and others. All OSS, all self-hosted.

# The Info Dump

* [Matrix/Riot](matrix_riot.md)
* [Matrix/Riot How-To Docs](matrix_riot_how_to.md)
* [Matrix/Riot Checklists](matrix_riot_checklists.md)
* [Marketing Services](marketing.md)

# License

All work contained within this repo is licensed [Creative Commons Attribution 4.0 International License (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).

The full text of the license is also [here](CC-BY-4.0.txt).

# Contributing

If you'd like to contribute, please open a Pull Request with your contribution(s).

Note: by submitting a PR you agree to your content being licensed under Creative Commons Attribution 4.0 International License.
