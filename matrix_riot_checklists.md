# Matrix / riot.im Checklists

Just a few checklists based on existing setups

# New Users

- Register new user(s) on matrix server
    - ```register_new_matrix_user -c /etc/matrix-synapse/homeserver.yaml http://localhost:8008```
- Invite new user(s) to ```Help/Support``` chat room
- Invite new user(s) to ```The Diner``` chat room
- Invite new user(s) to appropriate family communities

# Communities

- Create community via riot.im web
    - Invite only is usually good
- Create non-federated ```General``` room for the immediate family
    - Disable guest access as well
- Add avatar for community
    - Family crest / symbology is usually good
- Add global ```Help/Support``` chat room to the community
- Add global ```The Diner``` chat room to the community
- Invite existing users to the community
