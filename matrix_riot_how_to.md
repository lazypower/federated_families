# How To Riot.im

{:toc}

# Mobile

## Login

Logging in on mobile devices

1. Head to [https://about.riot.im/](https://about.riot.im/) and download the client for your mobile phone. There are links at the bottom of the page.
1. Launch the app on your phone
1. Enter your username and password (*DO NOT LOGIN YET!*)
1. Tick ```Use custom server optoins (advanced)```
1. Enter ```Home Server``` that was given to you by your administrator
1. Enter ```Identity Server``` that was given to you by your administrator
1. tap ```Log in```

### Example

![What needs to be selected for custom homeserver / identity server](img/matrix_riot/mobile_login.png "Log in on mobile")

## Set a display name and e-mail address

Setting a display name and adding an e-mail address

1. Click the gear icon to enter the settings
1. Enter your display name (this is what your name will show as to other users)
1. Enter an email address and click the ```+``` icon next to it
1. Verify your e-mail address (see ```E-mail Verification``` section below for further details)

### Example

![Display Name and Add email address web client fields](img/matrix_riot/email_display_name.png "Add Display Name / E-mail Address")

## General Settings to Adjust

Some additional settings you may want to adjust on mobile

1. Click the gear icon
1. Scroll down through the settings until you see ```Send anon crash & usage data``` and ```Rage shake to report bug```
1. Turn these settings off

### Example

![Anon data / Rage Shake options](img/matrix_riot/other_settings.png "Disable usage data / rage shake")

## iOS (Apple) Share Image Setup

How to setup image sharing on an iOS device (iPhone / iPad)

1. Open an image you'd like to share
1. Select the standard ```Share Image``` option
1. Scroll to the far right of the list of apps
1. Tap the ```More``` icon
1. Enable riot.im in the list that shows

### Examples

![iOS image share enable, more button](img/matrix_riot/ios_image_share_enable_1.png "More Button")

![iOS image share enable, enable riot.im](img/matrix_riot/ios_image_share_enable_2.png "Riot.im in list")

## Sharing an image

Small/Medium/Large/Actual Size. You probably want ```Actual Size``` ;)

1. Select an image to share
1. Select the standard ```Share Image``` option
1. Select ```riot.im```
1. When prompted, select ```Actual Size``` unless you want to share a smaller image for some reason

### Examples

![Sharing image, select riot.im](img/matrix_riot/ios_share_image.png "Riot.im in sharing selection")

![Sharing image, size selection](img/matrix_riot/ios_share_image_2.png "Size Selection")

# riot.im Web Interface

How to change your password, set a display name and add an e-mail address via the riot.im web interface.

## Password / Display Name Change

![Web password/display name change](img/matrix_riot/passwordchange.png "Web password/display name change")

## E-mail setup

![Web email setup](img/matrix_riot/emailsetup.png "Web email setup")

# E-mail Verification

What does e-mail verification look like?

## E-mail received after adding to riot.im

![Example verification e-mail](img/matrix_riot/verification_email.png "Example verification e-mail")

## Site shown after clicking the verify link

![Verification link clicked example](img/matrix_riot/verification_link_clicked.png "Verification link clicked example")
